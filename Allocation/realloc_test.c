#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned char menu(void){

    printf("Mostrar todos os valores.....[1]\n");
    printf("Inserir novo(s) valor(es)....[2]\n");
    printf("Sair.........................[3]\n");
    printf(">");

    unsigned char opc=getchar();

    fflush(stdin);

    return opc;
}

void show_values(const double *values, size_t n){

    double sum=0;

    printf("\n\n");

    if(n==0){

        printf("Atencao: Nenhum valor foi inserido\n\n");

    }else{

        for(size_t i=0; i<n; i++){

            sum+=values[i];
            printf("%d valor -> %lf\n", i+1, values[i]);
        }

        printf("\nMedia: %lf\n\n", sum/n);
    }
}

double *add_value(double *values, size_t *n){

    size_t q;
    double value;

    printf("\nInforme a quantidade de valores a serem inseridos >");
    scanf("%d", &q);

    values=realloc(values, (*n+q)*sizeof(double));

    printf("\n");

    for(size_t i=0; i<q; i++){

        printf("Informe o %d valor >", i+1);
        scanf("%lf", &value);

        values[i+(*n)]=value;
    }

    *n+=q;

    printf("\n\n%d novos valores foram adicionados.\n", q);
    printf("Total de valores presentes: %d.\n\n", *n);

    fflush(stdin);

    return values;
}

int main(void){

    size_t n=0;
    double *values=NULL;

    unsigned char opc;

    printf("\n\n");

    do{

        opc=menu();

        switch(opc){

            case '1':

                show_values(values, n);

                break;

            case '2':

                values=add_value(values, &n);

                break;

            case '3':

                printf("Saindo...\n\n");

                if(values!=NULL){

                    free(values);
                    values=NULL;
                }

                break;

            default:

                printf("Opcao incorreta\n\n");
                break;
        }

    }while(opc!='3');

    return 0;
}
